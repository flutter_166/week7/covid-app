import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class VaccineWidget extends StatefulWidget {
  VaccineWidget({Key? key}) : super(key: key);

  @override
  _VaccineWidgetState createState() => _VaccineWidgetState();
}

class _VaccineWidgetState extends State<VaccineWidget> {

  var vaccines = ['-', '-', '-'];

  @override
  void initState() { 
    super.initState();
    _loadVaccines();
  }

  _loadVaccines() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      vaccines =  prefs.getStringList('vaccines') ?? ['-', '-', '-'];
    });
  }

  _saveVaccines() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setStringList('vaccines', vaccines);
    });
  }

  Widget _vaccineComboBox({required String title, required String value, ValueChanged<String?> ? onChanged}) {
    return Row(
        children: [
          Text(title),
          Expanded(
            child: Container(),
          ),
          DropdownButton(
            items: [
              DropdownMenuItem(child: Text('-'), value: '-'),
              DropdownMenuItem(child: Text('Pfizer'), value: 'Pfizer'),
              DropdownMenuItem(
                  child: Text('Johnson & Johnson'), value: 'Johnson & Johnson'),
              DropdownMenuItem(child: Text('Sputnik V'), value: 'Sputnik V'),
              DropdownMenuItem(
                  child: Text('AstraZeneca'), value: 'AstraZeneca'),
              DropdownMenuItem(child: Text('Novavax'), value: 'Novavax'),
              DropdownMenuItem(child: Text('Sinophram'), value: 'Sinophram'),
              DropdownMenuItem(child: Text('Sinovac'), value: 'Sinovac'),
            ],
            value: value,
            onChanged: onChanged,
          )
        ],
    );
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Vaccine'),
      ),
      body: Container(
      padding: EdgeInsets.all(16.0),
      child:ListView(
        children: [
          _vaccineComboBox(
              title: 'เข็ม 1',
              value: vaccines[0],
              onChanged: (newValue) {
                setState(() {
                  vaccines[0] = newValue!;
                });
              }),
          _vaccineComboBox(
              title: 'เข็ม 2',
              value: vaccines[1],
              onChanged: (newValue) {
                setState(() {
                  vaccines[1] = newValue!;
                });
              }),
          _vaccineComboBox(
              title: 'เข็ม 3',
              value: vaccines[2],
              onChanged: (newValue) {
                setState(() {
                  vaccines[2] = newValue!;
                });
              }),
          ElevatedButton(onPressed: () {
            _saveVaccines();
            Navigator.pop(context);
          }, 
          child: Text('Save')
          )
        ],
      )),
    );
  }
}